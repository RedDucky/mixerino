#include <Arduino_FreeRTOS.h>
static void write_led(void* pvParameters);
static void read_pots(void* pvParameters);

char output[10];

void setup() {
	Serial.begin(9600);
	/** Set output pins **/
	pinMode(2,OUTPUT);	
	pinMode(3,OUTPUT);	
	pinMode(4,OUTPUT);	
	pinMode(5,OUTPUT);	
	pinMode(6,OUTPUT);	
	pinMode(7,OUTPUT);	
	pinMode(8,OUTPUT);	
	pinMode(9,OUTPUT);	
	/**
	 * task prototype from freeRTOS
	 * xTaskCreate(MyTask_pointer, "task_name", 100, Parameter, Priority, TaskHandle);
	**/
	xTaskCreate(write_led, "Write_LED", 100, NULL, 1, NULL);
	xTaskCreate(read_pot0, "Read_Pot0", 100, NULL, 1, NULL);
	xTaskCreate(read_pot1, "Read_Pot1", 100, NULL, 1, NULL);
	xTaskCreate(read_pot2, "Read_Pot2", 100, NULL, 1, NULL);
	xTaskCreate(read_pot3, "Read_Pot3", 100, NULL, 1, NULL);
}

void loop()
{
}


static void write_led(void* pvParameters)
{
	while(1){
		if ( Serial.available() ) {
			char rec = Serial.read();
			//Serial.println(rec);
			rec = rec - '0';
			if ( (rec >= 0) && (rec <= 8) ){
				for( char i=0; i<=8; i++ ) {
					if ( i < rec ) { 
						digitalWrite(9-i,HIGH);
					} else {
						digitalWrite(9-i,LOW);
					}
				}
			}
		}
		vTaskDelay(1);
	}
}

static void read_pot0(void* pvParameters)
{
	static int pot0;
	static int prev_pot0;
	int diff0;
	while(1){
		pot0 = analogRead(A0);
		diff0 = pot0 - prev_pot0;
		if ( (diff0 < -2) || (diff0 > 2) ) {
			prev_pot0 = pot0;
			sprintf(output, "0:%d", pot0);
			Serial.println(output);
		}
		vTaskDelay(1);
	}
}

static void read_pot1(void* pvParameters)
{
	static int pot1;
	static int prev_pot1;
	int diff1;
	while(1){
		pot1 = analogRead(A1);
		diff1 = pot1 - prev_pot1;
		if ( (diff1 < -2) || (diff1 > 2) ) {
			prev_pot1 = pot1;
			sprintf(output, "1:%d", pot1);
			Serial.println(output);
		}
		vTaskDelay(1);
	}
}

static void read_pot2(void* pvParameters)
{
	static int pot2;
	static int prev_pot2;
	int diff2;
	while(1){
		pot2 = analogRead(A2);
		diff2 = pot2 - prev_pot2;
		if ( (diff2 < -2) || (diff2 > 2) ) {
			prev_pot2 = pot2;
			sprintf(output, "2:%d", pot2);
			Serial.println(output);
		}
		vTaskDelay(1);
	}
}

static void read_pot3(void* pvParameters)
{
	static int pot3;
	static int prev_pot3;
	int diff3;
	while(1){
		pot3 = analogRead(A3);
		diff3 = pot3 - prev_pot3;
		if ( (diff3 < -2) || (diff3 > 2) ) {
			prev_pot3 = pot3;
			sprintf(output, "3:%d", pot3);
			Serial.println(output);
		}
		vTaskDelay(1);
	}
}
