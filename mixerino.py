import time
from math import floor

import pulsectl
import serial

pulse = pulsectl.Pulse('mixerino')
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=0.01)
sink = pulse.sink_list()[0]
SINK_INPUTS = pulse.sink_input_list()
NUM_SI = len(SINK_INPUTS)
pot_n = 1
for si in SINK_INPUTS:
    print(pot_n, si.name)
    pot_n += 1
TIMEOUT = 0.05
volume = sink.volume

for i in range(7):
    ser.write(str(i).encode())
    time.sleep(0.1)

while True:
    peak = pulse.get_peak_sample(sink.monitor_source, TIMEOUT)
    #print(vol)
    led_loud = str(floor(peak/0.125))
    ser.write(led_loud.encode())
    if ser.in_waiting:
        #print(ser.in_waiting)
        ser_byte = ser.readline()
        rec = ser_byte[0:len(ser_byte)-2].decode("utf-8")
        #print(rec)
        pot = int(rec[0])
        vol = float(rec[2:])
        if pot == '0':
            volume.value_flat = vol/1023
            pulse.volume_set(sink, volume)
        elif 1 <= pot <= NUM_SI:
            SINK_INPUTS[pot-1].volume.value_flat = vol/1023
            pulse.volume_set(SINK_INPUTS[pot-1], SINK_INPUTS[pot-1].volume)
